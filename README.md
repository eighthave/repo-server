
# repo-server

Calyx runs lots of F-Droid repos, this is the server that manages them and runs
related automation.


## System requirements

* Debian 11 (bullseye)
* ssh to root possible
* amd64/ARM/x86


## Deployment to remote server

You simply need to run the playbook from this project to a remote
host.  First you'll need to install ansible, on your local machine
e.g.:

    sudo apt-get install ansible

You'll also need to make sure that the bare minimum for ansible
is installed on your remote host:

    sudo apt-get install python3 sudo

Then you can immediately start the deployment:

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook -i remoteHost, provision.yml
