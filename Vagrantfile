Vagrant.configure("2") do |config|

  config.vm.box = "debian/bullseye64"
  config.vm.synced_folder '.', '/vagrant', disabled: true

  config.vm.provider :libvirt do |libvirt|
    libvirt.cpus = 2
    libvirt.memory = 1024
  end

  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "provision.yml"
  end

  config.vm.provision :shell, inline: <<-SHELL
    export LC_ALL=C.UTF-8
    export DEBIAN_FRONTEND=noninteractive
    set -ex
    apt-get update
    # standard Calyx VM https://gitlab.com/CalyxOS/calyxos/-/issues/756#note_727336575
    apt-get -qy install --no-install-recommends \
        acl \
        anacron \
        atool \
        bc \
        bind9-libs \
        binfmt-support \
        bridge-utils \
        bsdmainutils \
        console-setup \
        console-setup-linux \
        curl \
        dcfldd \
        dctrl-tools \
        dialog \
        dnsutils \
        dosfstools \
        dstat \
        efibootmgr \
        foot-terminfo \
        fuse3 \
        git \
        git-man \
        gpg \
        gpg-agent \
        gpgconf \
        htop \
        iftop \
        ioping \
        iperf \
        iptraf-ng \
        ipxe \
        joe \
        kbd \
        keyboard-configuration \
        kitty-terminfo \
        linux-perf \
        linux-perf-5.10 \
        lzop \
        mariadb-common \
        mlocate \
        mosh \
        myrepos \
        mysql-common \
        ncal \
        ncdu \
        netcat-openbsd \
        nload \
        nmon \
        parallel \
        parted \
        pexec \
        pinentry-curses \
        psmisc \
        pv \
        python3-dnspython \
        ranger \
        rename \
        reptyr \
        rsync \
        sshfs \
        sysbench \
        sysstat \
        tmux \
        tree \
        unzip \
        usbutils \
        vim \
        vim-runtime \
        whois \
        xfsprogs \
        xkb-data \
        yadm \
        zip \
        zlib1g \

SHELL

  config.vm.provision :shell, inline: <<-SHELL
    cp /home/vagrant/.ssh/authorized_keys /home/fdroid/.ssh/
    chown -R fdroid /home/fdroid/.ssh/
SHELL

end
